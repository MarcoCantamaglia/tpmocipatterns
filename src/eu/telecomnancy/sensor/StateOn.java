package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements ISensorState {
	private double value; 
	
	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}

}
