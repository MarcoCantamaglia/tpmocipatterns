package eu.telecomnancy.sensor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

public class ProxyTemperatureSensor extends Observable implements ISensor{
	private ISensor sensor; 
	
	public ProxyTemperatureSensor(ISensor sensor){
		this.sensor = sensor;
	}
	
	@Override
	public void on() {
		System.out.println("methode on");
		this.sensor.on();
		setChanged();
		notifyObservers();
	}

	@Override
	public void off() {
		System.out.println("methode off");
		this.sensor.off();	
		setChanged();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		System.out.println("methode getstatus");
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		System.out.println("methode getupdate");
		this.sensor.update();	
		setChanged();
		notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		System.out.println("methode getvalue");
		return this.sensor.getValue();
	}

}
