package eu.telecomnancy.sensor;

import java.util.Observable;

public abstract class SensorDecorator extends Observable implements ISensor{
	protected ISensor sensor;
	
	public SensorDecorator(ISensor sensor){
		this.sensor = sensor;
	}
	
	@Override
    public void on() {
        this.sensor.on();
        setChanged();
        notifyObservers();
    }

    @Override
    public void off() {
        this.sensor.off();
        setChanged();
        notifyObservers();
    }
    
    @Override 
    public void update() throws SensorNotActivatedException {
		this.sensor.update();	
		setChanged();
		notifyObservers();
	}
    
    @Override
    public boolean getStatus() {
		return this.sensor.getStatus();
    }
}
