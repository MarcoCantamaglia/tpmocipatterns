package eu.telecomnancy.sensor;

public class ArrondiDecorator extends SensorDecorator {

	public ArrondiDecorator(ISensor sensor) {
		super(sensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		int a = (int)(sensor.getValue()*100);
		return ((double) a)/100;
	}

}
