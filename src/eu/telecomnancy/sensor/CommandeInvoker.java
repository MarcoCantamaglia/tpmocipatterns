package eu.telecomnancy.sensor;

public class CommandeInvoker {
	private ISensor sensor;
	public CommandeInvoker(ISensor sensor){
		this.sensor=sensor;
	}
	
	public void execute(Commandes cmd) throws SensorNotActivatedException{
		cmd.execute(sensor);
	}
}
