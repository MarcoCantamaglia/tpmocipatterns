package eu.telecomnancy.sensor;

public class AdapterLegacyTemperatureSensor implements ISensor {

	private double value;
	private LegacyTemperatureSensor sensor;
	
	public AdapterLegacyTemperatureSensor(LegacyTemperatureSensor ls){
		value=0;
		sensor = ls;
	}
	
	@Override
	public void on() {
		if (!getStatus())
			sensor.onOff();		
	}
	@Override
	public void off() {
		if (getStatus())
			sensor.onOff();
	}
	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}
	@Override
	public void update() throws SensorNotActivatedException {
		if (getStatus())
			value = sensor.getTemperature();
		else
			throw new SensorNotActivatedException("Sensor must be activated to get its value.");		
	}
	@Override
	public double getValue() throws SensorNotActivatedException {
		if (getStatus())
			return value;
		else
			throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	
	
}
