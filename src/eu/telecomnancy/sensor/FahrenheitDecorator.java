package eu.telecomnancy.sensor;

public class FahrenheitDecorator extends SensorDecorator {

	public FahrenheitDecorator(ISensor sensor) {
		super(sensor);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue()*1.8+32;
	}

}
