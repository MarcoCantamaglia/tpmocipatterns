package eu.telecomnancy.sensor;

public class FactorySensor {
	 
	
	public static ISensor createTemperatureSensor(){
		return new TemperatureSensor();
	}

	public static ISensor createStateSensor(){
		return new StateTemperatureSensor();
	}

	public static ISensor createLegacySensor(){
		return new AdapterLegacyTemperatureSensor(new LegacyTemperatureSensor());
	}

	public static ISensor createProxySensor(){
		return new ProxyTemperatureSensor(new TemperatureSensor());
	}

}	
