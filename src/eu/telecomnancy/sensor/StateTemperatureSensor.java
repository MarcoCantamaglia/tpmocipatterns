package eu.telecomnancy.sensor;

import java.util.Observable;

public class StateTemperatureSensor extends Observable implements ISensor{

	private ISensorState state = new StateOff() ;

	@Override
	public void on() {
		state = new StateOn();
		setChanged();
		notifyObservers();
	}

	@Override
	public void off() {
		state = new StateOff();	
		setChanged();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		return this.state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.state.update();	
		setChanged();
		notifyObservers();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.state.getValue();
	}
	
}
