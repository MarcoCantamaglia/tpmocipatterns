package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.ArrondiDecorator;
import eu.telecomnancy.sensor.CommandeInvoker;
import eu.telecomnancy.sensor.CommandeOff;
import eu.telecomnancy.sensor.CommandeOn;
import eu.telecomnancy.sensor.CommandeUpdate;
import eu.telecomnancy.sensor.FahrenheitDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorView extends JPanel implements Observer{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ISensor sensor;
	private CommandeInvoker cmdInvoker;
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton celcius = new JButton("�C");
    private JButton fahren = new JButton("�F");
    private JButton round = new JButton("Arrondir");

    public SensorView(ISensor c) {
        this.sensor = c;
        this.cmdInvoker = new CommandeInvoker(sensor);
        ((Observable) this.sensor).addObserver(this);
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);

        final SensorView sv = this;
        
        
        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
					cmdInvoker.execute(new CommandeOn());
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				};
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
					cmdInvoker.execute(new CommandeOff());
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				}
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
					cmdInvoker.execute(new CommandeUpdate());
				} catch (SensorNotActivatedException e1) {
					e1.printStackTrace();
				};
            }
        });
        
        celcius.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	if (sensor instanceof FahrenheitDecorator || sensor instanceof ArrondiDecorator)
            		sensor = new TemperatureSensor();
            		((Observable) sensor).addObserver(sv);
            }
        });
        
        fahren.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor = new FahrenheitDecorator(sensor);
               ((Observable) sensor).addObserver(sv);
            }
        });
        
        round.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor = new ArrondiDecorator(sensor);
                ((Observable) sensor).addObserver(sv);
            }
        });
        

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 5));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(celcius);
        buttonsPanel.add(fahren);
        buttonsPanel.add(round);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		ISensor sensor;
		sensor = (ISensor) arg0;
		String degree;
		try {
			if (sensor instanceof FahrenheitDecorator)
				degree = Double.toString(sensor.getValue()) + "�F";
			else
				degree = Double.toString(sensor.getValue()) + "�C";
		} catch (SensorNotActivatedException e) {
			degree = "N/A °C";
			//e.printStackTrace();
		}
		value.setText(degree);
	}
}
