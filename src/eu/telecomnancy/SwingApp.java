package eu.telecomnancy;

import eu.telecomnancy.sensor.FactorySensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	ISensor sensor = FactorySensor.createProxySensor();
    	//ISensor sensor = new ArrondiDecorator(new TemperatureSensor());
    	//ISensor sensor = FactorySensor.createTemperatureSensor();
    	//ISensor sensor = FactorySensor.createStateSensor();
    	//ISensor sensor = FactorySensor.createLegacySensor();
    	new MainWindow(sensor);
    }

}
